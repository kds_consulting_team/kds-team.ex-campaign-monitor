import os
import sys
import csv
import logging
import requests
# import json
# import pandas as pd
from pandas.io.json import json_normalize
# import logging_gelf.handlers
# import logging_gelf.formatters
from keboola import docker


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
sys.tracebacklimit = 0

DEFAULT_TABLE_INPUT = '/data/in/tables/'
DEFAULT_TABLE_OUTPUT = '/data/out/tables/'
ENDPOINT = 'https://api.createsend.com/api/v3.2/'

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)-8s : [line:%(lineno)3s] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

"""
logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
)
logging_gelf_handler.setFormatter(
    logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])
"""

# Access the supplied rules
cfg = docker.Config('/data/')

try:
    params = cfg.get_parameters()
    token = params['#token']
except KeyError as e:
    logging.error("Unable to load parameter %s" % e)
    sys.exit(2)

auth_tuple = (token, '')
# Authentication function


def authenticate():
    """
    A dummy function testing the connectivity by downloading
    a clients table.

    Args:
        token (str): a string API token for Campaign Monitor API

    Returns:
        Information on authentication status.
        clients (dict): If authentication returns 200, a dict of clients is returned.
    """
    _endpoint = ENDPOINT + 'clients.json'
    logging.info("Authenticating...")
    client_request = requests.get(_endpoint, auth=auth_tuple)

    if client_request.status_code == 200:
        logging.info("Log in successful. Clients data downloaded.")
        clients = json_normalize(client_request.json())

        if clients.shape[0] == 0:
            logging.warn(
                "Client list is empty, no further data can be downloaded.")
            sys.exit(1)
        else:
            clients.to_csv(DEFAULT_TABLE_OUTPUT + 'clients.csv', index=False)
            return clients
    elif client_request.status_code == 401:
        logging.info(
            "Unable to log in. Please check, whether API token provided is correct.")
        sys.exit(1)
    else:
        logging.info("Unable to log in. Exited with error %s: %s" % (client_request.status_code,
                                                                     client_request.json()['Message']))
        sys.exit(2)


def clients_endpoint(client_list):
    _client_endpoint = ENDPOINT + 'clients/'

    for _, row in client_list.iterrows():
        cid = row['ClientID']
        campaigns_request = requests.get(
            _client_endpoint + cid + '/campaigns.json', auth=auth_tuple)
        list_request = requests.get(
            _client_endpoint + cid + '/lists.json', auth=auth_tuple)

        if len(campaigns_request.json()) == 0:
            logging.warn("No campaigns present for client %s." % row['Name'])
            continue

        tmp_campaigns = json_normalize(campaigns_request.json())
        tmp_campaigns['ClientID'] = [cid] * tmp_campaigns.shape[0]
        if 'campaigns' not in locals():
            campaigns = tmp_campaigns
        else:
            campaigns = campaigns.append(tmp_campaigns, ignore_index=True)

    if campaigns.shape[0] == 0:
        logging.warn(
            "No campaigns could be downloaded for any client. No further data is present. Exiting.")
        sys.exit(1)
    else:
        campaigns.to_csv(DEFAULT_TABLE_OUTPUT + 'campaigns.csv', index=False)
        return campaigns


def campaigns_summary(campaigns_list):
    _campaign_endpoint = ENDPOINT + 'campaigns/'

    for _, row in campaigns_list.iterrows():
        cid = row['CampaignID']

        summary_request = requests.get(_campaign_endpoint + cid + '/summary.json',
                                       auth=auth_tuple)

        tmp_summary_campaigns = json_normalize(summary_request.json())
        tmp_summary_campaigns['CampaignID'] = [cid]

        if 'campaign_summary' not in locals():
            campaign_summary = tmp_summary_campaigns
        else:
            campaign_summary = campaign_summary.append(
                tmp_summary_campaigns, ignore_index=True)

    campaign_summary.to_csv(DEFAULT_TABLE_OUTPUT +
                            'campaigns_summary.csv', index=False)


def campaign_stats(campaigns_list):
    _campaign_endpoint = ENDPOINT + 'campaigns/'

    for _, row in campaigns_list.iterrows():
        cid = row['CampaignID']
        logging.debug("Getting info for campaign %s" % cid)
        bounces_request = requests.get(
            _campaign_endpoint + cid + '/bounces.json', auth=auth_tuple)
        opens_request = requests.get(
            _campaign_endpoint + cid + '/opens.json', auth=auth_tuple)
        clicks_request = requests.get(
            _campaign_endpoint + cid + '/clicks.json', auth=auth_tuple)
        # unsubscribes_request = requests.get(_campaign_endpoint + cid + '/unsubscribes.json', auth=auth_tuple)
        # spam_request = requests.get(_campaign_endpoint + cid + '/spam.json', auth=auth_tuple)
        logging.debug("Data for campaign %s downloaded." % cid)

        if len(bounces_request.json()['Results']) != 0:
            tmp_bounces = json_normalize(bounces_request.json()['Results'])
            tmp_bounces['CampaignID'] = [cid] * tmp_bounces.shape[0]

            if 'bounces' not in locals():
                bounces = tmp_bounces
            else:
                bounces = bounces.append(tmp_bounces, ignore_index=True)
        logging.info("Bpunces saved.")
        if len(opens_request.json()['Results']) != 0:
            tmp_opens = json_normalize(opens_request.json()['Results'])
            tmp_opens['CampaignID'] = [cid] * tmp_opens.shape[0]

            if 'opens' not in locals():
                opens = tmp_opens
            else:
                opens = opens.append(tmp_opens, ignore_index=True)
        logging.info("Opens saved.")
        if len(clicks_request.json()['Results']) != 0:
            tmp_clicks = json_normalize(clicks_request.json()['Results'])
            tmp_clicks['CampaignID'] = [cid] * tmp_clicks.shape[0]

            if 'clicks' not in locals():
                clicks = tmp_clicks
            else:
                clicks = clicks.append(tmp_clicks, ignore_index=True)

    if 'opens' in locals():
        opens.to_csv(DEFAULT_TABLE_OUTPUT + 'opens.csv', index=False)

    if 'bounces' in locals():
        bounces.to_csv(DEFAULT_TABLE_OUTPUT + 'bounces.csv', index=False)

    if 'clicks' in locals():
        clicks.to_csv(DEFAULT_TABLE_OUTPUT + 'clicks.csv', index=False)


def campaigns_ListAndSegments(campaigns_list):
    _listsegments_endpoint = ENDPOINT + 'campaigns/%s/listsandsegments.json'

    with open('/data/out/tables/lists.csv', 'w') as campaigns_lists:
        fieldnames = ['CampaignID', 'ListID', 'Name']

        writer = csv.DictWriter(campaigns_lists, fieldnames=fieldnames)
        writer.writeheader()

        for _, row in campaigns_list.iterrows():
            cid = row['CampaignID']
            logging.info("Getting segments and lists for CampaignID %s." % cid)

            lists_request = requests.get(_listsegments_endpoint % cid, auth=auth_tuple)

            lists = lists_request.json()['Lists']

            for l in lists:
                l['CampaignID'] = cid
                writer.writerow(l)


def lists():
    _active_endpoint = ENDPOINT + 'lists/%s/' + 'active.json'
    _unsubb_endpoint = ENDPOINT + 'lists/%s/' + 'unsubscribed.json'
    _unconf_endpoint = ENDPOINT + 'lists/%s/' + 'unconfirmed.json'
    _bouncd_endpoint = ENDPOINT + 'lists/%s/' + 'bounced.json'

    with open('/data/out/tables/lists.csv', 'r') as lists, open('/data/out/tables/subscribers.csv', 'w') as subscribers:
        reader = csv.DictReader(lists)
        fieldnames = ['ListID',
                      'EmailAddress',
                      'Name',
                      'Date',
                      'State',
                      'CustomFields',
                      'ReadsEmailWith',
                      'ConsentToTrack']
        writer = csv.DictWriter(subscribers, fieldnames=fieldnames)
        writer.writeheader()

        for row in reader:
            list_id = row['ListID']

            logging.info("Getting subscriber information for list %s." % list_id)
            active_request = requests.get(_active_endpoint % list_id, auth=auth_tuple)
            unsubb_request = requests.get(_unsubb_endpoint % list_id, auth=auth_tuple)
            unconf_request = requests.get(_unconf_endpoint % list_id, auth=auth_tuple)
            bouncd_request = requests.get(_bouncd_endpoint % list_id, auth=auth_tuple)

            for sub in active_request.json()['Results']:
                sub['ListID'] = list_id
                writer.writerow(sub)

            logging.info("Active subscribers saved.")

            for sub in unsubb_request.json()['Results']:
                sub['ListID'] = list_id
                writer.writerow(sub)

            logging.info("Usubscribed subscribers saved.")

            for sub in unconf_request.json()['Results']:
                sub['ListID'] = list_id
                writer.writerow(sub)

            logging.info("Unconfirmed subscribers saved.")

            for sub in bouncd_request.json()['Results']:
                sub['ListID'] = list_id
                writer.writerow(sub)

            logging.info("Bounced subscribers saved.")


def main():
    clients = authenticate()
    campaigns = clients_endpoint(clients)
    campaign_stats(campaigns)
    campaigns_ListAndSegments(campaigns)
    lists()


if __name__ == '__main__':
    main()
