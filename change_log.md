**0.1.3**
Added endpoint, that obtains information about subscribers.

**0.1.1**
FLake8 compliance

**0.1.0**
Initial version of the component. Automatically fetches campaigns, clicks,
bounces and opens.