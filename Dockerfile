FROM quay.io/keboola/docker-custom-python:latest

COPY . /code/
#COPY /data/ /data/

RUN pip install flake8

RUN pip install -r /code/requirements.txt

WORKDIR /code/


CMD ["python", "-u", "/code/src/component.py"]
